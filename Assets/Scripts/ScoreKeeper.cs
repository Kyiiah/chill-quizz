using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeeper : MonoBehaviour
{

    int correctAnswer = 0;
    int questionSeen = 0;

    public int GetAnswer()
    {
        return correctAnswer;

    }

    public void IncrementCorrectAnswer()
    {
        correctAnswer++;
    }

    public int GetQuestionSeen()
    {
        return questionSeen;
    }

    public void IncrementQuestionSeen()
    {
        questionSeen++;

    }

    public float CalculateScore()
    {
        return Mathf.RoundToInt((correctAnswer / (float)questionSeen) * 100);
        

    }
}
