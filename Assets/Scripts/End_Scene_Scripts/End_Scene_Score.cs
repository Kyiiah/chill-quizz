using TMPro;
using UnityEngine;

public class End_Scene_Score : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] TextMeshProUGUI commentaire;
    [SerializeField] TextMeshProUGUI message;


    ScoreKeeper ScoreKeeper;

    private void Start()
    {
        ScoreKeeper ScoreKeeper = FindObjectOfType<ScoreKeeper>();
        float score = ScoreKeeper.CalculateScore();

        if (score == 0)
        {
            message.text = " T'es s�r d'avoir jou� l� ? ";

            scoreText.text = "Ton score est de " + score + "%";
            commentaire.text = " Aucun effort K�vin !! ";
        }

        else if (score > 0 && score <= 25)
        {
            message.text = " Bon... T'auras essay� mais... ";

            scoreText.text = "Ton score est de " + score + "%";
            commentaire.text = " T'es nul K�vin !! ";

        }

        else if (score > 25 && score <= 50)
        {
            message.text = " Merci d'avoir jou� !";
            scoreText.text = "Ton score est de " + score + "%";
            commentaire.text = " Franchement, tu peux mieux faire K�vin ! ";

        }

        else if (score > 50 && score <= 75)
        {
            message.text = " Merci d'avoir jou� !";
            scoreText.text = "Ton score est de " + score + "%";
            commentaire.text = " �a va, c'est pas trop mal, K�vin ! ";

        }

        else if (score > 75 && score < 100)
        {
            message.text = " Merci d'avoir jou� !";
            scoreText.text = "Ton score est de " + score + "%";
            commentaire.text = " 'Il est l� K�vin !' ";

        }

        else if (score == 100)
        {
            message.text = " Merci d'avoir jou� !";
            scoreText.text = "Ton score est de " + score + "%";
            commentaire.text = " Ti� trop fort K�vin bien ouej ! ";

        }

        else

        {
            message.text = " Mais ? ";
            scoreText.text = "Ton score est de " + score + "%";
            commentaire.text = " Tias bidouill� mon jeu ou quoi K�vin ? ";

        }
    }

}
