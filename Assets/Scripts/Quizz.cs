using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Quizz : MonoBehaviour
{
    [Header("Questions")]
    [SerializeField] TextMeshProUGUI questionText; //on initialise la classe TextMeshProUGUI modifiable dans l'inspecteur pour y mettre le champ de text concern�
    [SerializeField] QuestionSO question; //on initialise la classe QuestionSO (notre autre script avec la m�thode GetQuestion) dans l'inspecteur pour y mettre le SO de la question qu'on veut
    [SerializeField] QuestionSO[] questionArray = new QuestionSO[4];

    [Header("Answer Buttons")]

    [SerializeField] GameObject[] answerButtons; // on initialise dans l'inspecteur un tableau qui contient les gamesobjects des bouttons pour y acc�der � la composante text de son enfant et y mettre la r�ponse associ�e.
    [SerializeField] Sprite correctAnswerSprite;
    [SerializeField] Sprite defautAnswerSprite;
    [SerializeField] Sprite incorrectAnswerSelectedSprite;
    [SerializeField] int currentQuestionIndex; // 1er �l�ment de la sortingList


    [Header("Timer")]

    [SerializeField] Timer timer;

    [Header("Timer")]

    [SerializeField] ScoreKeeper ScoreKeeper;
    [SerializeField] TextMeshProUGUI ScoreText;

    [Header("Slider")]

    [SerializeField] Slider slider;

    int numberOfQuestion;
    int awaitingQuestion;



    void Start()
    {
        currentQuestionIndex = 0;
        sortingList.Shuffle(4);
        GetNextQuestion();
        numberOfQuestion = questionArray.Length;
        slider.minValue = 0;
        slider.maxValue = questionArray.Length;

    }

    void Update()
    {
        TimerDecount();
        //     TestContenuListe();
        //  Debug.Log(ScoreKeeper.GetQuestionSeen());
        //  Debug.Log(AwaitingQuestion());

    }


    ///tester le contenu de ma liste + currentQuestionIndex

    void TestContenuListe()
    {
        string result = "List contents: ";

        foreach (var item in sortingList)
        {
            result += item.ToString() + ", ";
        }
        Debug.Log(result);

        Debug.Log(currentQuestionIndex);
    }

    ///      ///       /// 




    public void TimerDecount()
    {
        Image timerObjectImage = timer.GetComponent<Image>();
        timerObjectImage.fillAmount = timer.fillFraction;
    }

    public int AwaitingQuestion()
    {
        return (questionArray.Length - ScoreKeeper.GetQuestionSeen());

    }



    public void IncrementQuestion()
    {
        if (currentQuestionIndex < 3)
        {
            currentQuestionIndex += 1;
        }

    }

    List<int> sortingList = new List<int> { 0, 1, 2, 3 };

    public int ReturnCurrentQuestion()
    {
        int currentQuestion = sortingList[currentQuestionIndex]; //num�ro de la question, si currentQuestionIndex = 0 alors le num de la question est le premier �l�ment de la sortinglist ex (2,3,1,0) cest la question 2
        return currentQuestion;
    }


    public void DisplayQuestion()

    {
        //     SetDefaultButtonSprite();                     
        int currentQuestion = ReturnCurrentQuestion();

        questionText.text = questionArray[currentQuestion].GetQuestion(); //on remplace le texte (initialis� sous le nom questionText dans l'inspecteur) par la question pr�sente dans l'objet scriptable (SO) r�cup�r�e par la m�thode GetQuestion()
                                                                          //      Debug.Log(questionText.text);  // test pour voir si TextMeshProUGUI.text prend automatiquement le text du composant TextMeshPro de l'objet porteur du script (c'est bien le cas)

        for (int index = 0; index < answerButtons.Length; index++)  // pour un indice entier index, qui d�marre � 0 et tant qu'il est inf�rieur � 3, avec incr�mentation de 1 � chaque boucle
        {
            TextMeshProUGUI buttonText = answerButtons[index].GetComponentInChildren<TextMeshProUGUI>(); // on cr�e une variable de type TMP buttonText qui est le TMP de l'enfant du gameobjet pr�sent � l'indice 0 du tableau answerbutton

            buttonText.text = questionArray[currentQuestion].GetAnswer(index); // modifier le composant text du textmeshpro buttonText par la r�ponse associ�e � l'index avec la m�thode getAnwser()
        }

    }



    public void OnAnswerSelected(int index)

    {
        int currentQuestion = sortingList[currentQuestionIndex];

        Image buttonCurrentImage = answerButtons[index].GetComponent<Image>();
        int correctAnswerIndex = questionArray[currentQuestion].GetCorrectAnswerIndex();
        Image buttonCorrectAnswerImage = answerButtons[correctAnswerIndex].GetComponent<Image>();



        if (index == questionArray[currentQuestion].GetCorrectAnswerIndex())
        {
            questionText.text = "Bonne r�ponse !";
            buttonCurrentImage.sprite = correctAnswerSprite;
            ScoreKeeper.IncrementCorrectAnswer();
            ScoreKeeper.IncrementQuestionSeen();



        }
        else
        {
            buttonCurrentImage.sprite = incorrectAnswerSelectedSprite;
            questionText.text = "Erreur! La r�ponse �tait \n" + questionArray[currentQuestion].GetAnswer(correctAnswerIndex);
            buttonCorrectAnswerImage.sprite = correctAnswerSprite;
            ScoreKeeper.IncrementQuestionSeen();


        }

        ActualiseScoreText();
        ButtonState(false);
        timer.CancelTimer();

    }

    public void DisplayCorrectAnwserTimeOut(int index)
    {

        int currentQuestion = sortingList[currentQuestionIndex];


        Image buttonCurrentImage = answerButtons[index].GetComponent<Image>();
        int correctAnswerIndex = questionArray[currentQuestion].GetCorrectAnswerIndex();

        Image buttonCorrectAnswerImage = answerButtons[correctAnswerIndex].GetComponent<Image>();

        questionText.text = "Temps �coul�! La r�ponse �tait \n" + questionArray[currentQuestion].GetAnswer(correctAnswerIndex);


        buttonCorrectAnswerImage.sprite = correctAnswerSprite;


        ScoreKeeper.IncrementQuestionSeen();
        ActualiseScoreText();
        ButtonState(false);
    }

    public void ActualiseScoreText()
    {
        ScoreText.text = "Score = " + ScoreKeeper.CalculateScore() + " %";

    }

    public void GetNextQuestion()
    {
        if (AwaitingQuestion() > 0)
        {

            SetDefaultButtonSprite();
            ButtonState(true);
            timer.hasAnswered = false;
            DisplayQuestion();
            IncrementSlider();

        }
        //    else if (AwaitingQuestion() <= 0)
        //    {
        //        SceneManager.LoadScene("End_Scene",LoadSceneMode.Additive);
        //        HideCanvas();

        //    }




    }



    public void ButtonState(bool state)

    {
        for (int i = 0; i < answerButtons.Length; i++)
        {
            Button button = answerButtons[i].GetComponent<Button>();
            button.interactable = state;  //interagissable  
        }
    }

    public void SetDefaultButtonSprite()
    {

        for (int i = 0; i < answerButtons.Length; i++)

        {
            Image buttonImage = answerButtons[i].GetComponent<Image>();
            buttonImage.sprite = defautAnswerSprite;
        }


    }


    public void IncrementSlider()
    {
        slider.value++;
    }

    public void HideCanvas()
    {
        this.GetComponent<Canvas>().enabled = false;

    }


}

