using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShuffleQuestion
    { 
    public static void Shuffle<T> (this List<T> list, int shuffleAccuracy) // T = type, donc le type d'objet dans la liste ici int
        {

        for (int i = 0; i < shuffleAccuracy; i++)
        {
            int randomIndex = Random.Range(0, list.Count);

            T temp = list[randomIndex]; // temp est une variable locale int . temp prend la valeur du num de l'indice tir� au sort de la liste � m�langer
            list[randomIndex] = list[i]; // ce meme num devient le premier de la liste recr��e
            list[i] = temp; // ce meme num prend la valeur  
        }

        }

    }