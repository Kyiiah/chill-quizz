using UnityEngine;
using UnityEngine.SceneManagement;


public class Timer : MonoBehaviour
{

    [SerializeField] float timeToCompleteQuestion;
    [SerializeField] float timetoShowCorrectAnswer;

    [SerializeField] QuestionSO[] questionArray = new QuestionSO[4];

    [SerializeField] bool isAnsweringQuestion;
    public bool hasAnswered = false;


    float timerValue;
    public float fillFraction;

    public Quizz quizz;
    public QuestionSO question;

    void Start()
    {
        isAnsweringQuestion = true;
        timerValue = 5;

    }



    void Update()
    {

        UpdateTimer();
    }


    public void CancelTimer() //doit etre reli� au cript quizz avec quelque chose comme Timer (nom de la classe � initialiser) timer  puis timer.CancelTimer() � la m�thode OnAnswerSelected()
    {
        timerValue = 0;
        hasAnswered = true;

    }

    //public void UpdateTimer()
    //{
    //    timerValue -= Time.deltaTime;
    //    Debug.Log(timerValue);

    //    if (isAnsweringQuestion)  // s'il r�pond � la question
    //    {
    //        if (timerValue <= 0) // et si le temps est < 0 donc fin du temps, il r�pond plus donc isanswering sur false, et le timer devient celui de l'affichage de la rep correcte
    //        { 
    //            isAnsweringQuestion = false;
    //            timerValue = timetoShowCorrectAnswer;

    //        }
    //        else // s'il r�pond et que le temps est > 0 donc pas �coul�, on calcule la fraction pour faire avancer le timer temps de rep question
    //        {
    //            fillFraction = timerValue / timetoShowCorrectAnswer;
    //        }
    //    }
    //    else // si il n'est pas en train de rep � la question (isanswering = false) 
    //    {
    //        if (timerValue <= 0) // Si le temps est �coul� on reset le timer sur temps pour repondre, et on passe � l'�tat il repond � la question
    //        {
    //            isAnsweringQuestion = true;
    //            timerValue = timeToCompleteQuestion;
    //            loadNextQuestion = true;
    //        }
    //        else // si il est pas en train de rep, et que le temps est > 0 cest qu'il consulte la reponse, donc on calcule la fraction pour le timer affichage reponse
    //        {
    //            fillFraction = timerValue / timeToCompleteQuestion;

    //        }
    //    }
    //}

    //int currentQuestion = question.sortingList[currentQuestionIndex];  
    //questionText.text = questionArray[currentQuestion].GetQuestion();

    public bool continueTimer = true;
    public void UpdateTimer()

    {
        if (continueTimer)
        {
            {
                timerValue -= Time.deltaTime;
                //   Debug.Log(timerValue);


                int currentQuestion = quizz.ReturnCurrentQuestion();


                if (isAnsweringQuestion)

                {
                    if (timerValue > 0)
                    {
                        fillFraction = timerValue / timeToCompleteQuestion;
                    }
                    else if (hasAnswered == false) // si le temps arrive � 0 et qu'il a pas r�pondu
                    {
                        isAnsweringQuestion = false;
                        int correctAnswerIndex = questionArray[currentQuestion].GetCorrectAnswerIndex();
                        quizz.DisplayCorrectAnwserTimeOut(correctAnswerIndex);
                        timerValue = timetoShowCorrectAnswer;

                    }
                    else // si le temps arrive � 0 car il a r�pondu (via CancelTimer())
                    {
                        isAnsweringQuestion = false;
                        timerValue = timetoShowCorrectAnswer;

                    }
                }
                else
                {
                    if (timerValue > 0)
                    {
                        fillFraction = timerValue / timetoShowCorrectAnswer;
                    }
                    else
                    {
                        isAnsweringQuestion = true;
                        timerValue = timeToCompleteQuestion;
                        quizz.IncrementQuestion();
                        if (quizz.AwaitingQuestion() > 0)
                        {
                            quizz.GetNextQuestion();
                        }

                        if (quizz.AwaitingQuestion() == 0)
                        {
                            continueTimer = false;
                            SceneManager.LoadScene("End_Scene", LoadSceneMode.Additive);
                            quizz.HideCanvas();


                        }


                    }
                }
            }
        }
    }
}