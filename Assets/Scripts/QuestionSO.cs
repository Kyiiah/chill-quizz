using UnityEngine;

[CreateAssetMenu(menuName = "Quizz Question", fileName = "New Question")] // cr�� un nouveau objet cr�able dans le menu clique droit, cr�er. Ici le bouton pour cr�er s'appelle Quizz Question, et il cr�� un objet de type Scriptable Objet (stockeur) du nom de New question
public class QuestionSO : ScriptableObject
{
    [TextArea(2, 6)]  // d�finit la taille du champ dans l'inspecteur pour �crire la question
    [SerializeField] string question = " tapez votre question "; // cr�e un champ public pour �crire la question
    [SerializeField] string[] answers = new string[4]; //attention premier index d'un tableau = 0 donc ici 0,1,2,3
    [SerializeField] int correctAnswerIndex;


    public string GetQuestion()   // m�thode pour r�cup�rer l'object de typ string question
    {
        return question;
    }

    public int GetCorrectAnswerIndex()
    {
        return correctAnswerIndex;
    }

    public string GetAnswer(int index)

    {
        return answers[index];

    }


}


//public class Test    //depuis un autre script test
//{

//    QuestionSO questionSO;  // on met la classe (avec le nom qu'on a cr��e, ici questionSO) dont on r�cup�re la m�thode

//    void testA()   //on cr�e une nouvelle m�thode (void car ne renvoie rien) qui cr�e une variable de text string "questionText" qui utilise la m�thode GetQuestion() du script (de la classe) questionSO

//    {
//        string questionText = questionSO.GetQuestion();

//    }


//}
